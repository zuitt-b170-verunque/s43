let posts = [];
let count = 1;


// ADD Post Data
document.querySelector("#form-add-post").addEventListener("submit", (event) => {
    // preventDefault allows the webpage to continue executing the codes without the need to reload
    event.preventDefault();

    posts.push({
        id:count,
        title:document.querySelector("#txt-title").value,
        synopsis:document.querySelector("#txt-body").value,
    })
    count ++

    showPost(posts)
    console.log(count)
    console.log(posts)

    alert("Movies post successfully created")
})



// Show post
const showPost = (posts) =>{
    let postEntries = '';
    
	posts.forEach((post) => {
        postEntries += `
        <div id="post-${post.id}">
        <h3 id="post-title-${post.id}">${post.title}</h3>
        
        <p id="post-body-${post.id}">${post.synopsis}</p>
        
        <button onclick="editPost('${post.id}')">Edit</button>
        
        <button onclick="deletePost('${post.id}')">Delete</button>
        
        </div>
		`
	})
	document.querySelector("#div-post-entries").innerHTML = postEntries;
}



// Edit Post and Update
// edit

const editPost = (id) => {
    let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let synopsis = document.querySelector(`#post-body-${id}`).innerHTML;
    
	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = synopsis;
}


// update post
document.querySelector('#form-edit-post').addEventListener('submit', (event) =>{
    
    event.preventDefault();
    
	for (let i=0; i < posts.length; i++) {
        if (posts[i].id.toString() === document.querySelector('#txt-edit-id').value) {
            
            posts[i].title = document.querySelector('#txt-edit-title').value;
			posts[i].synopsis = document.querySelector('#txt-edit-body').value;
            
            
            showPost(posts)
			alert('Movie post updated.')
			break;
		}
	}
})

// ACTIVITY 
/* 
make the delete button work
pressing the delete button will delete the post and not the whole screen

filter method

*/

const deletePost = (id) => {
    posts = posts.filter((post)=>{
        if(posts.id === document.querySelector('#txt-edit-id').value){
            return post
        }
    });

    document.querySelector(`#post-${id}`).remove();
}
